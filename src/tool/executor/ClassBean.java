package tool.executor;

public class ClassBean {
	private String className;
	private String jarFile;
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getJarFile() {
		return jarFile;
	}
	public void setJarFile(String jarFile) {
		this.jarFile = jarFile;
	}
	@Override
	public String toString() {
		return "ClassBean [className=" + className + ", jarFile=" + jarFile + "]";
	}
	
}
