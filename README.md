# JreLiteTool

#### 项目介绍
用于精简JRE的Lib大小，导出可部署的JVM精简运行环境

#### 软件架构
软件架构说明


#### 安装教程

1. 克隆本项目到本地
2. eclipse右键点击“build.fxbuild”接着点击“Build Export FX Application”指令生成可执行的jar文件
3. 双击jar文件，或者命令输入java -jar xxx.jar执行程序

#### 使用说明

1. 点击选择JRE，选择JDK文件夹下的JRE文件夹
2. 点击选择JAR，选择你的程序jar包
3. 点击执行打包，稍等片刻就会把JRE文件夹中所有包都重组，去掉不使用的部分。
4. 点击执行精简，开始精简JRE文件夹。

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)