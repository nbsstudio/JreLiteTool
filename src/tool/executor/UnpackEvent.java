package tool.executor;

public abstract class UnpackEvent{
	public abstract void generateDone();
	
	public abstract void exDone();
	
	public abstract void delDone();
}