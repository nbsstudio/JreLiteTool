package tool.model;

import java.util.ArrayList;
import java.util.List;

import tool.executor.ClassBean;
/**
 * 主模型，用于流程各节点之间传递通用参数
 * @author cody
 *
 */
public class MainModel {
	public MainModel(){
		classList = new ArrayList<ClassBean>();
		executable = false;
		unpackOkey = false;
		inProgress = false;
	}
	private Boolean executable;
	private List<ClassBean> classList;
	private Boolean unpackOkey;
	private Boolean inProgress;
	private int mode;//0.桌面图形界面程序,1.命令行程序,2.WEB项目,3.微服务

	public Boolean getExecutable() {
		return executable;
	}

	public void setExecutable(Boolean executable) {
		this.executable = executable;
	}

	public List<ClassBean> getClassList() {
		return classList;
	}

	public void setClassList(List<ClassBean> classList) {
		this.classList = classList;
	}

	public Boolean getUnpackOkey() {
		return unpackOkey;
	}

	public void setUnpackOkey(Boolean unpackOkey) {
		this.unpackOkey = unpackOkey;
	}

	public Boolean getInProgress() {
		return inProgress;
	}

	public void setInProgress(Boolean inProgress) {
		this.inProgress = inProgress;
	}

	public int getMode() {
		return mode;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	
	
}
