package tool.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import tool.util.JdbcUtil;

public class FilterModel {

	/**
	 * 过滤器
	 * @param files
	 * @return
	 * @throws Exception
	 */
	public static  List<File> filter(List<File> files,FilterSwitch filterSwitch) throws Exception {
		if (files != null) {
			List<File> newFileObjs = new ArrayList<File>();
			for (File file : files) {
				if (canDel(file,filterSwitch)) {
					newFileObjs.add(file);
				}
			}
			return newFileObjs;
		}else{
			throw new Exception("Can not filte the files,cause import files is null");
		}
	}
	
	/**
	 * 过滤出允许被删除的文件
	 * @param file
	 * @return
	 */
	protected static boolean canDel(File file,FilterSwitch filterSwitch) {
		List<Map<String, Object>> floderFilterList = new JdbcUtil().queryForList("select * from filter where type = 'floder'");
		List<Map<String, Object>> fileFilterList = new JdbcUtil().queryForList("select * from filter where type = 'file'");
		
		String path = file.getPath().replace("\\", "/");
		String substring = file.getName();
		if(floderFilterList.size()>0) {
			for (Map<String, Object> map : floderFilterList) {
				if (path.indexOf(map.get("name").toString())>-1) {
					return false;
				}
			}
		}
//特定过滤
//		if (path.indexOf("java/io")>-1) {
//			return false;
//		}
//		if (path.indexOf("java/util")>-1) {
//			return false;
//		}
//		if (path.indexOf("java/text")>-1) {
//			return false;
//		}
//		if (path.indexOf("java/math")>-1) {
//			return false;
//		}
//		if (path.indexOf("java/lang")>-1) {
//			return false;
//		}
//		if (path.indexOf("util/locale")>-1) {
//			return false;
//		}
//		if (path.indexOf("sun/launcher")>-1) {
//			return false;
//		}
		if (filterSwitch.isEnableJavaFX() && path.indexOf("javafx/embed")>-1) {
			return false;
		}
		if (filterSwitch.isEnableNio() && path.indexOf("java/nio")>-1) {
			return false;
		}
		if (filterSwitch.isEnableSunUtil() && path.indexOf("sun/util")>-1) {
			return false;
		}
		if (filterSwitch.isEnableAWT() && (path.indexOf("java/awt")>-1 || path.indexOf("sun/awt")>-1)) {
			return false;
		}
		
		//文件过滤
		if(fileFilterList.size()>0) {
			for (Map<String, Object> map : fileFilterList) {
				if (substring.indexOf(map.get("name").toString())>-1) {
					return false;
				}
			}
		}
//
//		if (substring.indexOf("Pattern$")>-1) {
//			return false;
//		}
//		if (substring.indexOf("Format")>-1) {
//			return false;
//		}
//		if (substring.indexOf("Locale")>-1) {
//			return false;
//		}
//		if (substring.indexOf("Lang")>-1) {
//			return false;
//		}
//		if (substring.indexOf("ResourceBundle")>-1) {
//			return false;
//		}
//		if (substring.indexOf("Atomic")>-1) {
//			return false;
//		}
		switch (substring) {
		case "Consumer.class":
			return false;
		case "Locale.class":
			return false;
		default:
			return true;
		}
	}
}
