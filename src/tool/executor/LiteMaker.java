package tool.executor;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import tool.model.FilterSwitch;
import tool.model.MainModel;
import tool.util.JdbcUtil;

/**
 * 精简制作器
 * @author cody
 *
 */
public class LiteMaker {

	public static void doLite(String jreFloder,String buildFloder,MainModel model, FilterSwitch filterSwitch,boolean isTest,LiteMakerEvent levent) {
		Thread th = new Thread(()->{
			List<File> files = takeFiles(new File(jreFloder));
			levent.scanDone();
			//开始删除
			for (File file : files) {
				try {
					if (canDel(file.getName(),model,filterSwitch)) {
						levent.deleteStart(file.getPath());
						if (!isTest) {
							file.delete();
						}
					}
				} catch (Exception e) {
				}
			}
			//把rt.jar移动到lib文件夹
			
			File originfile = new File(buildFloder+File.separator+"rt.jar");
			File file = new File(jreFloder+File.separator+"lib"+File.separator+"rt.jar");
			try {
				Files.copy(Paths.get(originfile.getAbsolutePath()),Paths.get(file.getAbsolutePath()));
				levent.done();
			} catch (IOException e) {
				levent.copyError(e);
			}
		});
		th.start();
	}
	
	protected static List<File> takeFiles(File f){
		List<File> list = new ArrayList<>();
        if(f!=null){
            if(f.isDirectory()){
                File[] fileArray=f.listFiles();
                if(fileArray!=null){
                	for (File file : fileArray) {
                		//递归调用
                    	list.addAll(takeFiles(file));
					}
                }
            }
            else{
            	list.add(f);
            }
        }
        return list;
    }
	
	/**
	 * 检查文件是否能被删除
	 * @param name
	 * @param model 
	 * @param filterSwitch
	 * @return
	 */
	protected static boolean canDel(String name,MainModel model, FilterSwitch filterSwitch) {
		List<Map<String, Object>> binFilterList = new JdbcUtil().queryForList("select * from filter where type = 'bin'");
		if(binFilterList.size()>0) {
			for (Map<String, Object> map : binFilterList) {
				//scene为-1则都需要判断是否能删，大于-1则按对应场景判断是否需要删
				if(map.get("scene").equals(-1) && name.indexOf(map.get("name").toString())>-1) {
					return false;
				}else if (map.get("scene").equals(model.getMode()) &&  name.indexOf(map.get("name").toString())>-1) {
					return false;
				}
			}
		}
		//BIN文件夹
//		if (name.indexOf("java.exe")>-1) {
//			return false;
//		}
//		if (name.indexOf("java.dll")>-1) {
//			return false;
//		}
		if (filterSwitch.isEnableAWT()) {
			if (name.indexOf("awt")>-1) {
				return false;
			}
			if (name.indexOf("javaw.exe")>-1) {
				return false;
			}
		}
		if (filterSwitch.isEnableJavaFX()) {
			if (name.indexOf("javafx")>-1) {
				return false;
			}
			if (name.indexOf("javaw.exe")>-1) {
				return false;
			}
			if (name.indexOf("glass.dll")>-1) {
				return false;
			}
			if (name.indexOf("prism_common.dll")>-1) {
				return false;
			}
			if (name.indexOf("prism_d3d.dll")>-1) {
				return false;
			}
		}
		if (name.indexOf("fontmanager.dll")>-1) {
			return false;
		}
		if (name.indexOf("msvcr100.dll")>-1) {
			return false;
		}
//		if (name.indexOf("msvcr120.dll")>-1) {
//			return false;
//		}
		if (name.indexOf("net.dll")>-1) {
			return false;
		}
		if (name.indexOf("nio.dll")>-1) {
			return false;
		}
		if (name.indexOf("t2k.dll")>-1) {
			return false;
		}
		if (name.indexOf("verify.dll")>-1) {
			return false;
		}
		if (name.indexOf("zip")>-1) {
			return false;
		}
		if (name.indexOf("jvm")>-1) {
			return false;
		}
		//LIB文件夹
		if (name.indexOf("charsets")>-1) {
			return false;
		}
		if (name.indexOf("currency")>-1) {
			return false;
		}
		if (name.indexOf("resources")>-1) {
			return false;
		}
		if (filterSwitch.isEnableJavaFX() && name.indexOf("jfxrt.jar")>-1) {
			return false;
		}
		return true;
	}
}
