package tool.executor;

public abstract class LiteMakerEvent {

	public abstract void scanDone();
	public abstract void deleteStart(String file);
	public abstract void done();
	public abstract void copyError(Exception e);
}
