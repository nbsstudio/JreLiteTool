package tool.executor;

public abstract class PackerEvent {

	public abstract void addOne(String row);
	public abstract void addDone(String jarFileName);
	
}
