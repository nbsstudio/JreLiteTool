package tool.executor;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * 重新打包并覆盖的执行器
 * @author cody
 *
 */
public class Packer {
	private PackerEvent event;
	public Packer(PackerEvent event){
		this.event= event;
	}

	public void packJar(String jreFloder,String jarFileName,String buildFloder) {
		Thread th = new Thread(()->{
			String param = "*";
			String command =new File(jreFloder).getParent()+ "\\bin\\jar cvf \""+jarFileName+"\" "+param;
			System.out.println(command);
			Process p;
			try {
				p = Runtime.getRuntime().exec(command,null, new File(buildFloder));
				// 读取命令的输出信息
				InputStream is = p.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(is));
				//p.waitFor();
//			if (p.exitValue() != 0) {
//				// 说明命令执行失败
//				// 可以进入到错误处理步骤中
//			}
				// 打印输出信息
				String s;
				while ((s = reader.readLine()) != null) {
					event.addOne(s);
				}
				event.addDone(jarFileName);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});	
		th.start();
	}
}
