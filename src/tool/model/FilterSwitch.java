package tool.model;

/**
 * 过滤器开关
 * 属性是对JAVA功能的保留与否做出指示
 * @author cody
 *
 */
public class FilterSwitch {
	
	public FilterSwitch(){
		this.enableJavaFX = true;
		this.enableSunUtil=false;
	}

	private boolean enableJavaFX;
	private boolean enableNio;
	private boolean enableAWT;
	private boolean enableSunUtil;

	public boolean isEnableJavaFX() {
		return enableJavaFX;
	}

	public void setEnableJavaFX(boolean enableJavaFX) {
		this.enableJavaFX = enableJavaFX;
	}

	public boolean isEnableNio() {
		return enableNio;
	}

	public void setEnableNio(boolean enableNio) {
		this.enableNio = enableNio;
	}

	public boolean isEnableAWT() {
		return enableAWT;
	}

	public void setEnableAWT(boolean enableAWT) {
		this.enableAWT = enableAWT;
	}

	public boolean isEnableSunUtil() {
		return enableSunUtil;
	}

	public void setEnableSunUtil(boolean enableSunUtil) {
		this.enableSunUtil = enableSunUtil;
	}
	
}
